FROM node:12.13.0-alpine
RUN apk update && apk add bash
WORKDIR /usr/src
ENV host_ip=localhost
COPY package*.json ./

RUN npm install
COPY . .

EXPOSE 3000
CMD  bash config.sh $host_ip && npm start
